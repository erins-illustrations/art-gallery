def parse_date(line):
    return line.split("[")[1].split(":")[0]

import subprocess
DOCKER_ID = "605606c2913f"
HIT_CHECK = "/ui/html/shop.html"
NEWLINE = "\n"

logs = subprocess.check_output(["docker", "logs", DOCKER_ID])
hit_lines = list(filter(lambda k: HIT_CHECK in k, logs.split(NEWLINE)))

dates = {}

for line in hit_lines:
    date = parse_date(line)
    if date in dates:
        dates[date] += 1
    else:
        dates[date] = 1

for date in sorted(dates.keys()):
    print("{}: {}".format(date,dates[date]))