portfolio.factory('homeService', ['$http', function($http) {
  var _getFile = function(url) {
    return $http.get(url);
  };

  return { getFile : _getFile };



}]);
