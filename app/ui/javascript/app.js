var portfolio = angular.module('gallery', ['ngRoute', 'hc.marked']).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/gallery', {templateUrl: 'ui/html/gallery.html', controller: 'galleryController', category: 'gallery', title: 'Gallery'});
  $routeProvider.when('/home', {templateUrl: 'ui/html/home.html', controller: 'homeController', category: 'home', title: 'Home'});
  // $routeProvider.when('/shop', {templateUrl: 'ui/html/shop.html', controller: 'shopController'})
  $routeProvider.otherwise({redirectTo: 'gallery'}); // should actually redirect to home when they are fixed
}]);
