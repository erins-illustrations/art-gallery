portfolio.controller('shopController', ['$scope', '$http', 'galleryService',
  function($scope, $http, galleryService) {

    $scope.row_iterators = ["$even", "$odd"]

    $scope.checkCommentLength = function() {
      LEN = 46
      if ($scope.comments.length > LEN)
        $scope.comments = $scope.comments.slice(0,LEN)
    }

    $scope.conversionRateUK = 1.25

    _getAbv = function(name) {
      _abbreviations = {
        "1 - Christmas Balls": "BAL",
        "10 - Christmas Elves": "ELV",
        "2 - Christmas Gnome": "GNO",
        "5 - Christmas Tree": "TRE",
        "7 - Dog Nativity": "DOG",
        "4 - Love Snowmen": "LOV",
        "9 - Polar Bears": "BEA",
        "3 - Reindeer": "REI",
        "8 - The Night Before": "NIG",
        "6 - Winter Cabin": "CAB"
      }
      return _abbreviations[name]
    }

    $scope.selectPackage = function(name) {
      if ($scope.selected_package == 'custom') {
        $scope.oldcart = Object($scope.cart)
      }
      $scope.selected_package = name
      if (name == 'all') {
        $scope.cart = {
          "Christmas Balls": 1,
          "Christmas Elves": 1,
          "Christmas Gnome": 1,
          "Christmas Tree": 1,
          "Dog Nativity": 1,
          "Love Snowmen": 1,
          "Polar Bears": 1,
          "Reindeer": 1,
          "The Night Before": 1,
          "Winter Cabin": 1
        }
        $scope.cartSize = 10
        $scope.packageDescription = 'I want 1 of everything'
      }
      else if (name == 'custom') {
        $scope.cart = Object($scope.oldcart)
        $scope.cartSize = _getNumCards()
        $scope.packageDescription = ''
      }
      $scope.updatePrice()
    }

    $scope.selectImage = function(image) {
      $scope.selectedimage = image
    }

    $scope.selectCard = function(card) {
      $scope.selectedcard = card
      $scope.selectImage(card.images[0])
    }

    _getOrderDescription = function() {
      s = ""
      if ($scope.packageDescription.length > 0)
        s += $scope.packageDescription
      else
        Object.keys($scope.cart).forEach(function(k) {
          v = $scope.cart[k]
          if (v > 0) {
            s = s + v + "x" + _getAbv(k) + ","
          }
        })
      s = s + "\nComments:" + $scope.comments
      return s
    }

    _getShippingCost = function() {
      if ($scope.order_type && $scope.order_type.includes('Shipped'))
        if ($scope.order_location == 'UK')
          return $scope.uk_shipping_rate
        else
          return $scope.us_shipping_rate
      return 0
    }

    $scope.setupShop = function() {
      $scope.getImages('christmas')
      $scope.price = '0.00'
      $scope.cart = {}
      $scope.oldcart = {}
      $scope.comments = ""
      $scope.us_shipping_rate = 10.0
      $scope.uk_shipping_rate = 2.50
      $scope.selected_package = "custom"
      $scope.cartSize = 0
      $scope.packageDescription = ""
      $scope.order_location = 'US'
      $scope.displayPrice = '0.00'
      $scope.money_symbol = '$'

      $scope.rates = [
        {cards: 1, cost: 4.50},
        {cards: 4, cost: 4.25},
        {cards: 6, cost: 4},
        {cards: 10, cost: 3.5}
      ]

      //  4.50 dollars a card for 3 cards or less
      //  4.25 a card for 4-5
      //  4.00 a card for 6-9
      //  3.50 for 10+

      // uk->us conversion rate
      // 1 GBP = 1.25253 USD

      $scope.updatePayment()
    }

    _getNumCards = function() {
      quantities = Object.values($scope.cart)
      if (quantities.length == 0)
        return 0
      return quantities.reduce(function(total, num) { return total + num; })
    }

    $scope.getPrice = function() {
      numcards = _getNumCards()
      price = 0
      price_per_card = 0
      $scope.rates.forEach(function(r) {
        if (numcards >= r.cards)
          price_per_card = r.cost
      })
      price = price_per_card * numcards
      if (price > 0)
        price = price + _getShippingCost()

      if ($scope.order_location == 'UK')
        price = price * $scope.conversionRateUK 
      
      price = price.toFixed(2)

      paypalConversionRate = 1.25253

      $scope.displayPrice = price
      if ($scope.order_location == 'UK')
        $scope.displayPrice = ($scope.displayPrice / paypalConversionRate).toFixed(2)

      return price
    }

    $scope.getImages = function(category) {
      baseurl = 'data/'+category
      galleryService.getDirectory(baseurl).success(function(data) {
        $scope.cards = galleryService.parseDirList(data, baseurl);
        $scope.cards.forEach(function(card) {
          galleryService.getDirectory(card.url).success(function(data) {
            card.images = galleryService.parseDirList(data, card.url)
            card.images.sort().reverse()

            $scope.selectCard(card)
          })
        })
      });
    };

    $scope.updatePrice = function() {
      $scope.price = $scope.getPrice()
      $scope.updatePayment()
    }

    $scope.changeCart = function(name, number) {
      $scope.cart[name] = number;
      $scope.cartSize = _getNumCards()
      $scope.updatePrice()
    };

    $scope.setOrderLocation = function(location) {
      $scope.order_location = location
      if (location == 'UK') {
        $scope.money_symbol = '£'
      } else {
        $scope.money_symbol = '$'
      }
      $scope.updatePrice()
    }

    $scope.updatePayment = function() {
      $scope.createOrder = function(data, actions) {
        return actions.order.create({
          purchase_units: [{
            description: _getOrderDescription(),
            soft_descriptor: "Christmas Cards",
            amount: {
              currency_code: "USD",
              value: String($scope.price)
            }
          }]
        })
      }

      $scope.onApprove = function(data, actions) {
        return actions.order.capture().then(function(details) {
          alert('Hi ' + details.payer.name.given_name + '! Thanks for your order!')
        })
      }
    }

    $scope.setOrderType = function(orderType) {
      if (orderType == 'ShippedUS' || orderType == 'Delivered')
        $scope.setOrderLocation("US")
      else if (orderType == 'ShippedUK')
        $scope.setOrderLocation("UK")
      $scope.order_type = orderType
      $scope.updatePrice()
    }

    paypal.Buttons({
      createOrder: function(data, actions) { return $scope.createOrder(data, actions) },
      onApprove: function(data, actions) { return $scope.onApprove(data, actions) }}).render('#paypal-button-container')
  }]);

