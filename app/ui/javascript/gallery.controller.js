portfolio.controller('galleryController', ['$scope','$http', '$window', 'galleryService',
  function($scope,$http,$window, galleryService) {
    document.oncontextmenu =new Function("return false;")
    console.log($scope)

    // Need to be checking window size so put window in scope
    $scope.window = window

    $scope.setProject = function(project) {
      $scope.selectedProject = project
    }

    $scope.getImages = function(category) {
      baseurl = 'data/' + category
      galleryService.getDirectory(baseurl).success(function(data) {
        // Temporary hack to get this working on cloudfront because getting a directory listing via http doesn't work
        $scope.projects = [{"url":"data/gallery/1-Feathers/","title":"1-Feathers","displaytitle":"1 - Feathers","shorttitle":"Feathers","$$hashKey":"object:5","images":[{"url":"data/gallery/1-Feathers/Flamingo.png","title":"Flamingo","displaytitle":"Flamingo","shorttitle":"","$$hashKey":"object:17"},{"url":"data/gallery/1-Feathers/Hummingbird.png","title":"Hummingbird","displaytitle":"Hummingbird","shorttitle":"","$$hashKey":"object:18"},{"url":"data/gallery/1-Feathers/Maccaw.png","title":"Maccaw","displaytitle":"Maccaw","shorttitle":"","$$hashKey":"object:19"},{"url":"data/gallery/1-Feathers/Peacock.png","title":"Peacock","displaytitle":"Peacock","shorttitle":"","$$hashKey":"object:20"},{"url":"data/gallery/1-Feathers/z-Next_painting_coming_soon.png","title":"z-Next_painting_coming_soon","displaytitle":"z - Next painting coming soon","shorttitle":"","$$hashKey":"object:21"}]},{"url":"data/gallery/2-Creatures/","title":"2-Creatures","displaytitle":"2 - Creatures","shorttitle":"Creatures","$$hashKey":"object:6","images":[{"url":"data/gallery/2-Creatures/Alligator.png","title":"Alligator","displaytitle":"Alligator","shorttitle":""},{"url":"data/gallery/2-Creatures/Crab.png","title":"Crab","displaytitle":"Crab","shorttitle":""},{"url":"data/gallery/2-Creatures/Frog.png","title":"Frog","displaytitle":"Frog","shorttitle":""},{"url":"data/gallery/2-Creatures/Jellyfish.png","title":"Jellyfish","displaytitle":"Jellyfish","shorttitle":""},{"url":"data/gallery/2-Creatures/Kingfisher.png","title":"Kingfisher","displaytitle":"Kingfisher","shorttitle":""},{"url":"data/gallery/2-Creatures/Macaw.png","title":"Macaw","displaytitle":"Macaw","shorttitle":""}]},{"url":"data/gallery/3-Food_Waste_Stories/","title":"3-Food_Waste_Stories","displaytitle":"3 - Food Waste Stories","shorttitle":"Food Waste_Stories","$$hashKey":"object:7","images":[{"url":"data/gallery/3-Food_Waste_Stories/hungary_plag.png","title":"hungary_plag","displaytitle":"hungary plag","shorttitle":""},{"url":"data/gallery/3-Food_Waste_Stories/John-p1.jpg","title":"John-p1","displaytitle":"John - p1","shorttitle":""},{"url":"data/gallery/3-Food_Waste_Stories/moldy_strawberry.png","title":"moldy_strawberry","displaytitle":"moldy strawberry","shorttitle":""},{"url":"data/gallery/3-Food_Waste_Stories/parliment.png","title":"parliment","displaytitle":"parliment","shorttitle":""},{"url":"data/gallery/3-Food_Waste_Stories/perfect_strawberry.png","title":"perfect_strawberry","displaytitle":"perfect strawberry","shorttitle":""},{"url":"data/gallery/3-Food_Waste_Stories/Red_heart.png","title":"Red_heart","displaytitle":"Red heart","shorttitle":""},{"url":"data/gallery/3-Food_Waste_Stories/wonky_veg.png","title":"wonky_veg","displaytitle":"wonky veg","shorttitle":""}]},{"url":"data/gallery/4-Landscapes/","title":"4-Landscapes","displaytitle":"4 - Landscapes","shorttitle":"Landscapes","$$hashKey":"object:8","images":[{"url":"data/gallery/4-Landscapes/Amazon.png","title":"Amazon","displaytitle":"Amazon","shorttitle":""},{"url":"data/gallery/4-Landscapes/Gobi.png","title":"Gobi","displaytitle":"Gobi","shorttitle":""},{"url":"data/gallery/4-Landscapes/Lake_Bled.png","title":"Lake_Bled","displaytitle":"Lake Bled","shorttitle":""},{"url":"data/gallery/4-Landscapes/London_Canal.png","title":"London_Canal","displaytitle":"London Canal","shorttitle":""},{"url":"data/gallery/4-Landscapes/Serengeti.png","title":"Serengeti","displaytitle":"Serengeti","shorttitle":""},{"url":"data/gallery/4-Landscapes/St.Kilda.png","title":"St","displaytitle":"St","shorttitle":""}]},{"url":"data/gallery/5-Christmas/","title":"5-Christmas","displaytitle":"5 - Christmas","shorttitle":"Christmas","$$hashKey":"object:9","images":[{"url":"data/gallery/5-Christmas/christmas_balls.png","title":"christmas_balls","displaytitle":"christmas balls","shorttitle":""},{"url":"data/gallery/5-Christmas/Christmas_Fireplace.png","title":"Christmas_Fireplace","displaytitle":"Christmas Fireplace","shorttitle":""},{"url":"data/gallery/5-Christmas/christmas_gnome.png","title":"christmas_gnome","displaytitle":"christmas gnome","shorttitle":""},{"url":"data/gallery/5-Christmas/christmas_tree.png","title":"christmas_tree","displaytitle":"christmas tree","shorttitle":""},{"url":"data/gallery/5-Christmas/reindeer.png","title":"reindeer","displaytitle":"reindeer","shorttitle":""},{"url":"data/gallery/5-Christmas/Snowy_Cabin.png","title":"Snowy_Cabin","displaytitle":"Snowy Cabin","shorttitle":""}]},{"url":"data/gallery/7-About/","title":"7-About","displaytitle":"7 - About","shorttitle":"About","$$hashKey":"object:10","images":[{"url":"data/gallery/7-About/About.jpg","title":"About","displaytitle":"About","shorttitle":""}]}]
        $scope.setProject($scope.projects[0])

        // Due to issue with cloudfront not allowing directory listings, we need to hardcode in the projects list for now
        // To update projects list
           // uncomment the below lines
           // run locally, open the console, and copy the JSON output
           // Paste into the $scope.projects line
           // Recomment
           // Refresh page to make sure it's working still

        // $scope.projects = galleryService.parseDirList(data, baseurl, categoryname=true)
        // $scope.setProject($scope.projects[0])
        // $scope.projects.forEach(function(prj) {
        //   galleryService.getDirectory(prj.url).success(function(data) {
        //     prj.images = galleryService.parseDirList(data, baseurl+"/"+prj.title).filter(function(src) { return src.url.includes(".jpg") || src.url.includes(".png") })
            
        //     if (prj.title=="1-Gallery") {
        //       prj.images= prj.images.sort(function(){return Math.random() - 0.5;})
        //     }
            
        //     prj.shorttitle = prj.shorttitle.replace("_"," ")

        //     console.log(JSON.stringify($scope.projects))
        //   })
        // })
        

    });};


  }]);
