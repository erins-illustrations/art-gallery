portfolio.factory('galleryService', ['$http', function($http) {
  var _getDirectory = function(url) {
    return $http.get(url);
  };

  var _getFile = function(url) {
    return $http.get(url)
  };

  var _parseDirList = function(data, baseurl, categoryname=null) {
    return data.split("\n").filter(function(line) {
      return line.startsWith("<li>");
    }).map(function(line) {
      temp = line.slice(5, line.length) // slice out <li>
      url = temp.slice(temp.indexOf(">")+1, temp.indexOf("<")).trim()
      title = url.slice(0, url.indexOf("."))
      shorttitle=""
      if (categoryname && title.indexOf("-") >= 0)
        shorttitle=title.slice(title.indexOf("-")+1)
      url = baseurl + "/" + url
      displaytitle = title.replaceAll("_"," ").replace("-", " - ")
      return {url: url, title: title , displaytitle: displaytitle, shorttitle: shorttitle }
    }).filter(function(src){ 
      return !src.url.includes('.DS_Store'); });
  }

  return { getDirectory : _getDirectory, getFile: _getFile, parseDirList: _parseDirList };

}]);
